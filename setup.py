from setuptools import setup, find_packages

setup(
    name='profile_preferences',
    version='0.0.1',
    author='John Leith',
    author_email='leith.john@gmail.com',
    packages=find_packages(),
    description='user preferences',
    install_requires=[
        "six"
    ],
    include_package_data=True,
    zip_safe=False,
)
