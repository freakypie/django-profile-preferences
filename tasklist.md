- get the user model, it may have been overridden

Suggestion for your preferences project: have the foreign key be to "AUTH_USER_MODEL", which is set like this: AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

- rename to 'user_preferences'
- fix documentation
