import json
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.generic.base import TemplateView, View
from profile_preferences.models import Preference


class PreferenceMixin(object):

    @method_decorator(login_required)
    @method_decorator(csrf_protect)
    def dispatch(self, request, *args, **kwargs):
        return super(PreferenceMixin, self).dispatch(request, *args, **kwargs)


class PreferencesView(PreferenceMixin, TemplateView):
    template_name = "profile_preferences/view.html"

    def get_context_data(self, **kwargs):
        return dict(
            preferences=Preference.groups(self.request.user)
        )


class PreferenceUpdateView(PreferenceMixin, View):

    def get_queryset(self):
        return Preference.objects.filter(user=self.request.user)

    def post(self, request):
        count = 0
        prefs = Preference.list(user=request.user)

        for pref in prefs.keys():
            if pref in request.POST:
                value = request.POST.get(pref)
                if value != prefs[pref].default:
                    Preference.objects.put(request.user, pref, value)
                else:
                    Preference.objects.erase(request.user, pref)
                count += 1

        return HttpResponse(json.dumps({
            "updated": count
        }), content_type="x-application/json")
