from django.contrib.auth.models import User
from django.shortcuts import resolve_url
from django.test.testcases import TestCase
from .models import Preference


class ProfilePreferenceTestCase(TestCase):

    def setUp(self):
        Preference.register("test", "test_string", default="foo")
        self.user = User.objects.create_user("user", "", "user")

    def test_add_preference_callable(self):
        def inner(user=None):
            return [dict(key="blah")]
        Preference.register("test_callable", inner)
        self.assertEqual(Preference.list()['blah'].key, "blah")

    def test_list_preferences(self):
        self.assertEqual(Preference.groups()['test']['test_string'].key, "test_string")
        self.assertEqual(Preference.groups()['test']['test_string'].default, "foo")
        self.assertEqual(Preference.list()['test_string'].key, "test_string")
        self.assertEqual(Preference.list()['test_string'].default, "foo")

    def test_user_preference(self):
        Preference.objects.put(self.user, "test_string", "test value")
        Preference.objects.get(
            user=self.user,
            key="test_string",
            value="test value"
        )
        self.assertEqual(
            Preference.objects.fetch(self.user, "test_string"),
            "test value"
        )

    def test_fetch_user_preference_default(self):
        self.assertEqual(
            Preference.objects.fetch(self.user, "test_string"),
            "foo"
        )
        self.assertEqual(
            Preference.objects.fetch(self.user, "test_string", "foo2"),
            "foo2"
        )

    def test_preference_view(self):
        self.client.login(username="user", password="user")
        response = self.client.get(resolve_url("profile_preferences"))
        self.assertEqual(response.status_code, 200)

        response = self.client.post(resolve_url("profile_preference_update"), {
            "test_string": "baz"
        })
        print(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Preference.objects.fetch(self.user, "test_string"), "baz")
