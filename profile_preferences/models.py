from django.forms.widgets import Select, CheckboxInput, TextInput, EmailInput, NumberInput
from operator import attrgetter
from collections import OrderedDict
from django.db import models


class PreferenceManager(models.Manager):

    def fetch(self, user, key, default=None):
        try:
            return Preference.objects.get(user=user, key=key).value
        except Preference.DoesNotExist:
            if default is None:
                option = Preference.list(user=user).get(key)
                if option and option.default:
                    default = option.default
            return default

    def put(self, user, key, value):
        print(user, key, value)
        preference, created = Preference.objects.get_or_create(
            user=user,
            key=key,
            defaults=dict(value=value)
        )
        if not created:
            preference.value = value
            preference.save()
        return preference

    def erase(self, user, key):
        try:
            Preference.objects.get(user=user, key=key).delete()
        except Preference.DoesNotExist:
            pass


class PreferenceItem(object):
    key = None
    group = None
    value = None
    type = "string"
    label = None
    help_text = ""
    choices = None
    default = None
    ordering = 0

    def __init__(self, key, **kwargs):
        self.key = key
        if "label" not in kwargs:
            kwargs['label'] = key
        for name, value in kwargs.items():
            setattr(self, name, value)

    @property
    def val(self):
        return self.value or self.default

    @property
    def input(self):
        if self.choices:
            widget = Select(choices=self.choices)
        elif self.type == "boolean":
            widget = CheckboxInput()
        elif self.type == "email":
            widget = EmailInput()
        elif self.type == "integer":
            widget = NumberInput()
        else:
            widget = TextInput()

        return widget.render(
            name=self.key,
            value=self.val
        )


class Preference(models.Model):
    user = models.ForeignKey("auth.User")
    key = models.CharField(max_length=50, unique=True)
    value = models.CharField(max_length=255)

    objects = PreferenceManager()
    preferences = {}

    @classmethod
    def register(cls, group, key, **kwargs):
        if group not in cls.preferences:
            cls.preferences[group] = []

        if callable(key):
            cls.preferences[group].append(key)
        else:
            cls.preferences[group].append(
                PreferenceItem(key, group=group, **kwargs)
            )

    @classmethod
    def list(cls, user=None):
        if user:
            initial = dict(
                Preference.objects.filter(user=user).values_list("key", "value")
            )
        else:
            initial = {}
        print(initial)
        preferences = OrderedDict()
        for group, prefs in cls.preferences.items():
            for pref in prefs:
                if callable(pref):
                    for p in pref(user):
                        if not isinstance(p, PreferenceItem):
                            p = PreferenceItem(**p)
                        p.group = group
                        p.value = initial.get(p.key, p.default)
                        preferences[p.key] = p
                else:
                    preferences[pref.key] = pref
        return preferences

    @classmethod
    def groups(cls, user=None):
        all_prefs = OrderedDict()
        for key, preference in cls.list(user).items():
            if preference.group not in all_prefs:
                all_prefs[preference.group] = []
            all_prefs[preference.group].append(preference)

        for group, preferences in all_prefs.items():
            preferences.sort(key=attrgetter("key"))
            preferences.sort(key=attrgetter("ordering"))
            all_prefs[group] = OrderedDict(
                [(pref.key, pref) for pref in preferences]
            )
        return all_prefs
