from django.contrib import admin
from profile_preferences.models import Preference


class PreferenceAdmin(admin.ModelAdmin):
    search_fields = ("key", "user__username")
    list_display = ("user", "key", "value")


admin.site.register(Preference, PreferenceAdmin)
