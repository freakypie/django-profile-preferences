from django.conf.urls import url, patterns
from profile_preferences.views import PreferencesView, PreferenceUpdateView

urlpatterns = patterns('',
    url('^preferences/$', PreferencesView.as_view(), name="profile_preferences"),
    url('^preferences/update/$', PreferenceUpdateView.as_view(), name="profile_preference_update"),
)
