Profile Preferences
===================

This app is intended to make it easy to add preferences to a user profile
without each app having to implement both the preference model
and a way to update it in the profile page. 

This app is not intended for global or site settings, all settings are based on a user


Installation
------------

add `profile_preferences` to your INSTALLED_APPS

TODO: show how to add URLS


Usage
-----

In your app's loading path, call `Profile.register`. Example:

    Profile.register("Newsletter", "send", type="boolean", default=True)

This will add a `Newsletter` group in the user's profile page and a single
preference "send".

TODO: show how to use callables